# Archivo
A wrapper script around various archivers. Sick of looking at the man pages for each archiver command every time you want to do something simple? archivo wraps the most common operations of archivers into a single command so you don't have to look for the correct command and option manually, and you can pass multiple archives as arguments to operate on all of them.

# Usage
```sh
archivo file1 file2 file3  # Fzf selection of which archiver to use to archive files
archivo -a archive_name file1 file2 file3  # Giving name to the archive with an argument
archivo -o directory file1 file2 file3  # Giving an output directory
archivo -l archive1.7z archive2.zip   # List files of archive(s)
archivo -x archive1.ar archive2.gzip  # Extract files of archive(s)
archivo -x *.tar*  # Extract tar-like archives
```

# Note
I wrote it before I knew about 'atool' which has more and mature command-line functionality, however I still find this useful for interactive use with fzf when archiving, automatically looping over archive arguments, and because I have trouble creating an archive with directories with atool.

# License
GPLv3 or later
