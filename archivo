#!/bin/sh
# Un envolvedor para operar diferentes formatos de archivos

# If you need even more functionality checkout 'atool', but it doesn't work with directories

# TODO:
# Más formatos: lha, arj, arc, nomarch, p7zip, unalz, pbzip2, lzip, plzip, lzop, .egg, .whl, .jar, Apple .dmg (con hdiutil) y .sparseimage, Windows .msi
# Más opciones:
# --recursive  # Extraer archivos adentro de archivos extraídos
# --quiet
# --outdir  # Especificar un mismo lugar donde se archive/desarchive todos los pasados
# --samedir  # Especificar que se desarchive en el mismo directorio donde estén los argumentos pasados
# --wrap  # Para que al desarchivar, si son ficheros sueltos, ponerlos en un directorio
# --preserve # Para preservar atributos como permisos, si es posible con el compresor
# --pass-through  # Leer la lista de ficheros de stdin, pues demasiados argumentos causas un error
# --stdout  # Descomprimir a salida estándar
# Quizás usar 'file' para detectar el tipo compresión en vez de confiar en la extensión del fichero
# con esta forma: file -b --mime-type

. "${XDG_DATA_HOME%/*}/lib/guiones/getoptions.sh"

VERSION=0.1
# Olvidé por qué puse IFS así
IFS='
'
CCn='
'

fzf_cmd(){ fzf --prompt "${1:-"Seleccione: "}";}

archivar() {
	case "$1" in
		--ask) Opt_ask=1; shift ;;
		'') return 6 ;;
		*) archivo="$1"; shift ;;
	esac

	_IFS="$IFS"
	unset IFS

	archivadores="ar tar zip 7z rar lha" # compress
	compresores="gzip bzip2 xz lzma lzip lzop lrzip zstd" # lz4, lrz, rzip

	for archivador in $archivadores; do
		if hash "$archivador" >/dev/null 2>&1; then
			case "$archivador" in
				tar)
				archivador_tar=1
				for compresor in $compresores; do
					if hash "$compresor" >/dev/null 2>&1; then
						case "$compresor" in
							gzip) archivador="$archivador${CCn}tar+gz";;
							bzip2) archivador="$archivador${CCn}tar+bz2";;
							xz) archivador="$archivador${CCn}tar+xz";;
							lzma) archivador="$archivador${CCn}tar+lzma";;
							lzip) archivador="$archivador${CCn}tar+lzip";;
							lzop) archivador="$archivador${CCn}tar+lzop";;
							lrzip) archivador="$archivador${CCn}tar+lrzip";;
							zstd) archivador="$archivador${CCn}tar+zst";;
						esac
					fi
				done
				;;
				7z) archivador_7z=1 ;;
			esac
			lista="${lista}${CCn}${archivador}" lista="${lista#"${CCn}"}"
		fi
	done
	if [ $((archivador_tar & archivador_7z)) -eq 1 ]; then
		lista="${lista}${CCn}tar+7z"
	fi

	compactificador=$(fzf_cmd "Seleccione compactificador: " <<-EOF
	$lista
	EOF
	) || return 1

	ask_name() {
		printf "Escriba un nombre para su archivo: "
		read -r REPLY
		case "$REPLY" in
			'') ask_name;;
			*) archivo="$REPLY"
		esac
	}
	[ "$Opt_ask" ] && ask_name

	case "$compactificador" in
		ar) ar -rs "${Opt_o}${archivo}.a" "$@" ;;
		tar) tar -cf "${Opt_o}${archivo}.tar" "$@" ;;
		tar+gz) tar -czf "${Opt_o}${archivo}.tar.gz" "$@" ;;
		tar+bz2) tar -cjf "${Opt_o}${archivo}.tar.bz2" "$@" ;;
		tar+xz) tar -cJf "${Opt_o}${archivo}.tar.xz" "$@" ;;
		tar+lzma) tar -c --lzma -f "${Opt_o}${archivo}.tar.lzma" "$@" ;;
		tar+lzip) tar -c --lzip -f "${Opt_o}${archivo}.tar.lzip" "$@" ;;
		tar+lzop) tar -c --lzop -f "${Opt_o}${archivo}.tar.lzop" "$@" ;;
		tar+zst) tar -c --zstd -f "${Opt_o}${archivo}.tar.zst" "$@" ;;
		tar+7z) tar cf - "$@" | 7za a -si "${Opt_o}${archivo}.tar.7z" ;;
		zip) zip -r "${Opt_o}${archivo}.zip" "$@" ;;
		rar) rar a "${Opt_o}${archivo}.rar" "$@" ;;
		lha) lha ao7 "${Opt_o}${archivo}.lzh" "$@" ;;
		7z) 7z a "${Opt_o}${archivo}.7z" "$@" ;;
		#lrzip) lrzip -o "${archivo}.lrz" "$@" ;;
		tar+lrzip)
			#if [ $# -eq 1 ] && [ -d "$1" ]; then
				#lrztar "${archivo:-"$1"}"
			#else
				{
				cd "${Opt_o}"
				mkdir "${archivo}"
				cp "$@" "${archivo}"
				lrztar "${archivo}"
				rm -r "${archivo}"
				cd -
				} || return 2
			#fi
			;;
		#compress) ;;
	esac

	IFS="$_IFS"
}

listar() {
	# TODO: poder pasar una opcion que sea 'silenciosa' para que solo
	# se vean los ficheros, sin el resto de la verbosidad del archivador
	for archivo do
		archivo=$(realpath "$archivo")
		if [ -f "$archivo" ]; then
			case "${archivo%,}" in
			*.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*tar.zst|*.tar|*lzip|*lzop)
				tar -tf "$archivo";;
			*.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)  # también .lzh?
				7z l "$archivo";;
				#7z l ./"$archivo" | sed -n '/---/,/---/p' | cut -c 54- ;; # Silenciosamente, con GNU sed
			*.a)         ar -t "$archivo"            ;;
			*.rar)       rar l "$archivo"            ;;
			*.lzma)      unlzma -l "$archivo"        ;;
			*.cbz|*.epub|*.zip)  unzip -l "$archivo" ;;
			*.gz)        gunzip -l "$archivo"        ;;
			*.cpio)      cpio --list < "$archivo"    ;;
			*.exe)       cabextract -l "$archivo"    ;;
			*.xz)        unxz -l "$archivo"          ;;
			*.lzh)       lha l "${archivo}"          ;;
			*)  >&2 printf "Extensión de '\033[1m%s\033[m' desconocida\n" "$archivo"
				return 1 ;;
			esac
			#printf "\n"
		else
			>&2 printf "\033[1;31m'%s' no es un fichero!\n\033[1;32mSaltando...\033[m\n" "$archivo"
			continue
		fi
	done
}

extraer() {
	# TODO: refactorear esto, quizás primero encontrar la subextensión y luego destarear
	# zcat
	for archivo do
		#archivo=$(realpath "$archivo")
		if [ -f "$archivo" ]; then
			case "${archivo%,}" in
			*.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar.zst|*.tar.lzip|*.tar.lzop)
				tar xf "$archivo";;
			*.tar.lzma|*.tlzma)
				# cannot untar tar+lzma directly, it involves 2 steps and leaves the unlzma tar
				unlzma ./"$archivo"
				tar xf ./"${archivo%".lzma"}"
				;;
			*.tar.lrz)   lrzuntar "${Opt_o}$archivo"  ;;
			*.tar.7z)    7za x -so "${Opt_o}$archivo" | tar xf - ;;
			#*.lrzip)        unxz -l ./"$archivo"          ;;
			*.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
				7z x ./"$archivo" ;;
			*.a|*.ar)    ar -x ./"$archivo"         ;;
			*.lzma)      unlzma ./"$archivo"        ;;
			*.bz2)       bunzip2 ./"$archivo"       ;;
			*.cbr|*.rar) unrar x -ad ./"$archivo" || rar x ./"$archivo"  ;;
			*.gz)        gunzip ./"$archivo"        ;;
			*.cbz|*.epub|*.zip|*.egg|*.whl|*.jar)
				unzip -O shift-jis ./"$archivo" ;;
			*.z)         uncompress ./"$archivo"    ;;
			*.xz)        unxz ./"$archivo"          ;;
			*.exe)       cabextract ./"$archivo"    ;;
			*.cpio)      cpio -id < ./"$archivo"    ;;
			*.cba|*.ace) unace x ./"$archivo"       ;;
			*.lzh|*.lha) lha x ./"${archivo}"       ;;
			*)  >&2 printf "Extensión de '\033[1m%s\033[m' desconocida\n" "$archivo"
				return 1 ;;
			esac
		else
			>&2 printf "\033[1;31m'%s' no es un fichero!\n\033[1;32mSaltando...\033[m\n" "$archivo"
			continue
		fi
	done
}

agregar() {
	:
	# tar -rf "$archivo" "$@"
	# ar -r "$archivo" "$@"
	#cpio
}

remover() {
	:
	# tar --delete
}

info() {
	for archivo do
		#archivo=$(realpath "$archivo")
		if [ -f "$archivo" ]; then
			case "${archivo%,}" in
			*.lrz) lrzip -i "${archivo}" ;;
			*.zip) zipdetails "${archivo}" ;;
			*.xz) xz -l "${archivo}" ;;
			*)  >&2 printf "Extensión de '\033[1m%s\033[m' desconocida\n" "$archivo"
				return 1 ;;
			esac
		else
			>&2 printf "\033[1;31m'%s' no es un fichero!\n\033[1;32mSaltando...\033[m\n" "$archivo"
			continue
		fi
	done
}

parser() {
	setup REST help:uso -- "Envolvedor de diferentes archivadores."
	msg -- '' 'USO:' "  ${0##*/} [OPCIONES] [--] [ARCHIVOS | FICHEROS]"
	msg -- '' 'OPCIONES:'
	param Opt_a -a --archive -- "Archivar ficheros (por omisión)"
	flag Opt_x -x --extract -- "Extraer ficheros de archivos"
	flag Opt_l -l --list -- "Listar ficheros de archivos"
	flag Opt_i -i --info -- "Ver información de archivos"
	param Opt_o -o --outdir -- "Especificar el directorio de salida"
	disp :uso -h --help -- "Mostrar esta ayuda"
	disp VERSION --version -- "Mostrar versión"
}

eval "$(getoptions parser) exit 1"

main() {
	: "${Opt_o:=./}"  # Por omisión directorio de salida es el actual
	Opt_o="${Opt_o%/}/"  # Poner una barra final si no tiene

	if [ $# -eq 0 ]; then
		uso 1>&2
		return 1
	elif [ "$Opt_l" ]; then
		listar "$@"
	elif [ "$Opt_x" ]; then
		extraer "$@"
	elif [ "$Opt_i" ]; then
		info "$@"
	elif [ "$Opt_a" ]; then
		archivar "$Opt_a" "$@"
	else
		archivar --ask "$@"
	fi
}

main "$@" || exit $?
